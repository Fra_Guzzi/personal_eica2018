### README ###
"Elaborazione delle Immagini per la Chirurgia Assistita", repository class 2018

### OUTLINE ###
Notebook_0: Introduction to Python 

Notebook_1: Introduction to image procesing in Python

Notebook_2: Introduction to medical image processing in python 

Notebook_3: Fourier

Notebook_4: Convolution

Notebook_5: Dicom

Notebook_6: Morphological filters

Notebook_7: Image segmentation

Notebook_8: Digitally reconstructed radiography

Notebook_9: Segmentation metrics

Notebook_10: Triangulation

Notebook_11: Marching cubes

Notebook_12: Features

Notebook_13: Camera Calibration

