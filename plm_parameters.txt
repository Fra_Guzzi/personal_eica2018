[GLOBAL]
fixed=chest.nrrd
moving=chest_1.nrrd
vf_out=out_vf.nrrd
xform_out=out_xform.txt
img_out=out_img.nrrd

[STAGE]
xform=rigid
optim=versor
impl=plastimatch
metric=mse
max_its=100
convergence_tol=3
grad_tol=1
res=2 2 2

[STAGE]
xform=bspline
optim=lbfgsb
impl=plastimatch
res=1 1 1
grid_spac=35 35 35
max_its=100

[STAGE]
xform=bspline
optim=lbfgsb
impl=plastimatch
res=1 1 1
grid_spac=10 10 20
max_its=100
